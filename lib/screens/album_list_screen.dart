import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/album_list_provider.dart';

class AlbumListScreen extends StatefulWidget {
  const AlbumListScreen({Key? key}) : super(key: key);

  @override
  State<AlbumListScreen> createState() => _AlbumListScreenState();
}

class _AlbumListScreenState extends State<AlbumListScreen> {
  @override
  void initState() {
    super.initState();
    context.read<AlbumListProvider>().fetchAlbumList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Albums'),
      ),
      body: _buildList(context),
    );
  }

  Widget _buildList(BuildContext context) {
    final albumListProvider = context.watch<AlbumListProvider>();
    if (albumListProvider.albumList != null) {
      return ListView(
        children: [
          for (final album in albumListProvider.albumList!)
            ListTile(
              title: Text(album.title.toString()),
            ),
        ],
      );
    } else if (albumListProvider.error != null) {
      return Center(
        child: Text(albumListProvider.error.toString()),
      );
    } else {
      return const LinearProgressIndicator();
    }
  }
}
