import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/album_list_provider.dart';
import 'screens/album_list_screen.dart';
import 'services/api.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const api = API();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AlbumListProvider(api)),
      ],
      child: MaterialApp(
        title: 'Flutter Assessment - Albums',
        theme: ThemeData.from(
          colorScheme: const ColorScheme.light(
            primary: Colors.blue,
          ),
        ),
        home: const AlbumListScreen(),
      ),
    );
  }
}
