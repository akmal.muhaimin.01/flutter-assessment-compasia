import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

/// This class handles anything related to interaction with REST API.
class API {
  const API();

  /// Fetch JSON lists from a specified URL. The data fetched will be converted into Dart lists.
  ///
  /// Parameters:
  /// [url]: Direct URL link containing the JSON list.
  ///
  /// Returns:
  /// A future containing the JSON list.
  Future<List<Map<String, dynamic>>> getListFromUrl(String url) async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return (jsonDecode(response.body) as List).cast<Map<String, dynamic>>();
    } else {
      throw HttpException(
        'Failed to fetch album list: ${response.statusCode}, ${response.body}',
      );
    }
  }
}
