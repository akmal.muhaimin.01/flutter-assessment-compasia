import 'package:flutter/foundation.dart';

import '../models/album.dart';
import '../services/api.dart';

class AlbumListProvider with ChangeNotifier {
  final API api;

  /// The real contents of album list. Will be null for the first time running.
  /// Use [fetchAlbumList] to start the data fetching process.
  List<Album>? albumList;

  /// The error object if an exception occurs during fetching process.
  Object? error;

  AlbumListProvider(this.api);

  /// Fetches a demo data of albums list from JSONPlaceholder.
  /// All subscribers of this provider will be notified of the status change,
  /// and the data or error retrieved can be viewed on [albumList] or [error], respectively.
  void fetchAlbumList() async {
    try {
      final jsonList = await api
          .getListFromUrl('https://jsonplaceholder.typicode.com/albums');
      albumList = jsonList.map((e) => Album.fromMap(e)).toList();
    } catch (e) {
      error = e;
    } finally {
      notifyListeners();
    }
  }
}
